<?php

namespace Eshop\ClassesMetiers ;
use Eshop\DataBase\DBA ;

class Categorie
{
    private $idCategorie;
    private $libelle;
    private $image;
    private $collectionProduit = [];

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getIdCategorie()
    {
        return $this->idCategorie;
    }

    public function getLibelle()
    {
        return $this->libelle;
    }

    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    private static $select = "select * from categorie ";
    private static $selectById = "select * from categorie where idCategorie=:idCategorie";
    private static $insert = "insert into categorie (libelle, image) values (:libelle,:image)";
    private static $update = "update categorie set libelle=:libelle, image=:image where idCategorie = :idCategorie";

    public function save() {
        if (!$this->idCategorie) {
            $this->insert();
        } else {
            $this->update();
        }
        $this->saveProduit();
    }

    public function insert() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->execute();
        $this->idCategorie = $pdo->lastInsertId();
    }

    public function update() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$update);
        $pdoStatement->bindParam(":idCategorie", $this->idCategorie);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->execute();
    }

    public static function fetch($idCategorie) {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$selectById);
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        return Categorie::arrayToCategorie($record);
    }

    public static function fetchAll() {
        $categories = array();
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->query(Categorie::$select);
        $resultat = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        if (is_array($resultat)) {
            foreach ($resultat as $value) {
                $categories[] = Categorie::arrayToCategorie($value);
            }
        }
        return $categories;
    }

    private static function arrayToCategorie(Array $array) {
        $c = new Categorie();
        $c->idCategorie = $array["idCategorie"];
        $c->libelle = $array["libelle"];
        $c->image = $array["image"];
        $c->collectionProduit = Produit::fetchAllByCategorie($c);

        return $c;
    }

    public function compareTo(Categorie $categorie) {
        return $this->idCategorie == $categorie->idCategorie; // true ou false
    }

    private function existProduit(Produit $produit) {
        $exist = false;
        foreach ($this->collectionProduit as $produitCourant) {
            if ($produit->compareTo($produitCourant)) {
                $exist = true;
                break;
            }
        }

        return $exist;
    }

    private function saveProduit() {
        foreach($this->collectionProduit as $produit) {
            $produit->save();
        }
    }

    public function addProduit(Produit $produit) {
        if (!$this->existProduit($produit)) { // si il n'y a pas déjà le produit
            $this->collectionProduit[] = $produit; // alors on ajoute le produit
            if (is_a($produit->getCategorie(), "Categorie") && !$produit->getCategorie()->compareTo($this)) {
                // si la catégorie du produit est bien (de type Categorie) une catégorie ET que le produit n'a pas déjà la catégorie de ($this) l'objet
                $produit->setCategorie($this); // on assigne au produit la catégorie courante ($this).
            }
        }
    }

    public function removeProduit(Produit $produit) {
        $new = [];
        foreach ($this->collectionProduit as $produitCourant) {
            if (!$produitCourant->compareTo($produit)) { // si le produitCourant n'est pas le produit que l'on veut supprimer
                $new[] = $produitCourant; // on l'affecte au nouveau tableau
                break;
            }
        }
        $this->collectionProduit = $new; // le produit n'existe plus dans la catégorie
        $produit->setCategorie(null); // on enlève la référence de la catégorie ($this) du produit de la classe produit.
    }
}

<?php

namespace Eshop\ClassesMetiers ;
use Eshop\DataBase\DBA ;

class Produit {
    #region Attribut

    private $idProduit;
    private $libelle;
    private $description;
    private $prix;
    private $image;
    private $categorie;

    /*a) */private static $select = "select * from produit";
    /*b) */private static $selectById = "select * from produit where idProduit = :idProduit";
    /*c) */private static $insert = "insert into produit (libelle, description, image, prix, idCategorie) values (:libelle, :description, :image, :prix, :idCategorie)";
    /*d) */private static $update = "update produit set libelle = :libelle, description = :description, image = :image, prix = :prix, idCategorie = :idCategorie where idProduit = :idProduit";
    /*e) */private static $delete = "delete from produit where idProduit = :idProduit";

    /*
    a) La représentation textuelle de la requête SQL permet la sélection des enregistrements de la table
    « Produit ».

    b) La représentation textuelle de la requête SQL permet la sélection d’un enregistrement de la table
    « Produit » via un identifiant de produit. L’identifiant du produit n’est pas défini. « :idProduit » est
    un marqueur nommé, qui sera remplacé par une valeur lors de la préparation de la requête.

    c) La représentation textuelle de la requête correspond à l’insertion d’un produit dans la base de
    données.

    d) La représentation textuelle de la requête correspond à la mise à jour d’un produit dans la base de
    données identifié.

    e) La représentation textuelle de la requête correspond à la suppression d’un produit dans la base de
    données.
    */

    #endregion

    #region Propriété

    /**
     * @return mixed
     */
    public function getIdProduit()
    {
        return $this->idProduit;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getCategorie() {
        return $this->categorie;
    }

    /**
     * @return mixed $categorie
     */

    public function setCategorie(Categorie $categorie = null) { // permet de mettre une valeur par défaut null pour dissocier un produit et une catégorie
        $this->categorie = $categorie; // on affecte au produit la catégorie passés en paramètre
        if ($categorie != null) { // si la catégorie n'est pas null
            $categorie->addProduit($this); // on ajoute le produit à la collection de produit de la catégorie.
        }
    }

    #endregion

    #region Méthodes

    private static function arrayToProduit(Array $array) {
        $unProduit = new Produit();
        $unProduit->idProduit = $array["idProduit"];
        $unProduit->libelle = $array["libelle"];
        $unProduit->description = $array["description"];
        $unProduit->image = $array["image"];
        $unProduit->prix = $array["prix"];
        $idCategorie = $array["idCategorie"];
        if($idCategorie != null) {
            $unProduit->categorie = Categorie::fetch($idCategorie);
        }
        return $unProduit;
    }

    /*

        La méthode est privée : car il s’agit de fonctionnalités (traitements) qui ne peuvent (et ne doivent) pas
    être déclenchées directement par les utilisateurs de la classe.

    La méthode est à portée classe (static) : Le traitement n’est pas lié à une instance (objet) en particulier.
    Une méthode est définie « static » lorsque le traitement ne s’applique pas à l’objet courant.

    Cette méthode s’appuie sur les données transmises en paramètre via un tableau associatif pour
    initialiser un objet de type « Produit ». L’objet est ensuite retourné à l’appelant (une autre
    méthode…).

    L’accès au champ d’objet est réalisé à partir d’un tiret et d’un chevron. Vous
    remarquerez l’absence du signe « $ » devant le nom des champs.

    */

    /*
        La méthode qui suit permet à l’utilisateur de la classe « Produit » d’obtenir l’ensemble des objets de
    type « Produit ». Chaque objet de type « Produit » fait référence à un enregistrement dans la base de données.
    */

    public static function fetchAll() {
        $collectionProduit = array();
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->query(Produit::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduit($record);
        }
        return $collectionProduit;
    }

    public static function fetch($idProduit) {
        $produit = null ;
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectById);
        $pdoStatement->bindParam(":idProduit", $idProduit);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if(!$record) {
            $produit = Produit::arrayToProduit($record);
        }
        return $produit;
    }

    #region 3. METHODES DE MISE A JOUR DES DONNEES

    public function save() {
        if(!$this->idProduit) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    private function insert(){
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description",$this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if($this->categorie) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $this->idProduit = $pdo->lastInsertId();
    }

    private function update() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$update);
        $pdoStatement->bindParam(":idProduit", $this->idProduit);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description",$this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if($this->categorie) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
    }

    public function delete() {
        $dba = new DBA();
        $pdo = $dba->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$delete);
        $pdoStatement->bindParam(":idProduit", $this->idProduit);
        $resultat = $pdoStatement->execute();
        $nblignesAffectees = $pdoStatement->rowCount();
        if ($nblignesAffectees == 1) {
            $this->idProduit = null;
        }
        return $resultat;
    }
    #endregion

    #endregion


}




?>